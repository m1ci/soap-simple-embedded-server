# SOAP Embedded Server

This simple projects runs a Tomcat Web server and deploys a minimal SOAP Web service.
This project follows the CODE-FIRST approach.

## How to run?

* `mvn clean install cargo:run`
* the server will run at port `9090`
* the Web Service configuration is in: `WebContent/WEB-INF/sun-jaxws.xml`

## Test after running

* Summary page [http://localhost:9090/testWebService](http://localhost:9090/testWebService)
* WSDL at [http://localhost:9090/testWebService?wsdl](http://localhost:9090/testWebService?wsdl)

## Docker

In order to avoid incompatibility issues, it is highly recommend to run and test the solution with docker.
Before moving to the next steps make sure you have docker installed on your computer.

Here are the recommended steps:

**1. Create a shared folder in your filesystem and clone this repository in the folder.**

The contents of this folder will be shared between your computer (i.e. host) and the container.

Example:

`mkdir /Users/milan/Desktop/tmpvol`

`cd /Users/milan/Desktop/tmpvol`

`git clone git@bitbucket.org:m1ci/soap-simple-embedded-server.git`


**2. Build an image from the Dockerfile**

`docker build -t my-java-image .`

**3. Run a container from the image**

`docker run -it --mount type=bind,source=/Users/milan/Desktop/tmpvol,target=/app -p 9090:9090 --name my-java-image my-java-image /bin/bash`

This command will run the container and take you to the bash terminal of the container

Note:

* `--mount` - mounts the shared folder to a folder in the container (located in `/app`)

**4. Run the server and deploy your service**

* In the container terminal, go to the shared folder `cd /app`
* Then move the folder of the implementation `cd soap-simple-embedded-server`
* Finally, run the server `mvn clean install cargo:run`

**5. Test your service**

At [http://localhost:9090/testWebService](http://localhost:9090/testWebService) you should be able to see the service overview page.

## Tips

* The Web Service connfiguration is available at https://bitbucket.org/m1ci/soap-simple-embedded-server/src/master/WebContent/WEB-INF/sun-jaxws.xml
  * `services.TestService` - here `services` is the package, while `TestService` is the class name that implements the Web Service